using UnityEngine;

public class EventTest : MonoBehaviour {

    public GameObject obj;
    public Transform parent;
    public Renderer rend;

    private GameObject _spawendObj;
    private Collider coll;
    private Color defaultColor = new Color32(104, 0, 255, 255);

    void SpawnEvent()
    {
        Debug.Log("Spawning an object!");
        _spawendObj = Instantiate(obj, new Vector3(0, 1, 0), Quaternion.identity, parent);
    }

    void ToggleGravity()
    {
        foreach (Transform child in parent)
        {
            // Get the collider and set gravity appropriately
            coll = child.GetComponent<Collider>();
            if (coll.attachedRigidbody.useGravity == true)
            {
                coll.attachedRigidbody.useGravity = false;
                Debug.Log("Turning gravity off!");

                // Get the renderer and change the color to blue
                rend = child.GetComponent<Renderer>();
                rend.material.color = Color.blue;
            }
            else if (coll.attachedRigidbody.useGravity == false)
            {
                coll.attachedRigidbody.useGravity = true;
                Debug.Log("Turning gravity on!");

                // Get the renderer and change the color to purple (default)
                rend = child.GetComponent<Renderer>();
                rend.material.color = defaultColor;
            }
        }
    }

    void ColorYellow()
    {
        // Get the renderer and change the color to purple (default)
        rend = GetComponent<Renderer>();
        rend.material.color = Color.yellow;     
    }

    void ColorDefault()
    {
        // Get the renderer and change the color to purple (default)
        rend = GetComponent<Renderer>();
        rend.material.color = defaultColor;
    }
}
