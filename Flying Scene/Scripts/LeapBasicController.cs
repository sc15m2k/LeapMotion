﻿using UnityEngine;
using Leap;

public class LeapBasicController {

    Controller controller;

    public LeapBasicController()
    {
        controller = new Controller();
    }

    public Frame GetFrame()
    {
        Frame frame = controller.Frame();

        return frame;
    }

    // string handType = "left" or "right"
    public Hand GetHand(string handType)
    {
        Frame frame = GetFrame();

        Hand leftHand = null;
        Hand rightHand = null;

        if (frame != null)
        {
            // Debug.Log("Frame <" + frame.Id + "> is OK.");
            foreach (Hand hand in frame.Hands)
            {
                if (hand.IsLeft)
                {
                    leftHand = hand;
                }
                else
                {
                    rightHand = hand;
                }
            }
        }

        if (handType == "left")
            return leftHand;
        else if (handType == "right")
            return rightHand;
        else
            return null;
    }

    /// <param name="handType">"left", "right"</param>
    /// <param name="fingerType">"TYPE_THUMB", "TYPE_INDEX", "TYPE_MIDDLE", "TYPE_RING", "TYPE_PINKY"</param>
    /// <returns>Returns Finger type object</returns>
    /*
        Parameters:

        frameId - The id of the frame this finger appears in.
        handId - The id of the hand this finger belongs to.
        fingerId - The id of this finger (handId + 0-4 for finger position).
        timeVisible - How long this instance of the finger has been visible.
        tipPosition - The position of the finger tip.
        tipVelocity - The velocity of the finger tip.
        direction - The pointing direction of the finger.
        stabilizedTipPosition - The stabilized tip position.
        width - The average width of the finger.
        length - The length of the finger.
        isExtended - Whether the finger is more-or-less straight.
        type - The finger name.
        metacarpal - The first bone of the finger (inside the hand).
        proximal - The second bone of the finger
        intermediate - The third bone of the finger.
        distal - The end bone.
    */
    public Finger GetFinger(string handType, string fingerType)
    {
        Hand hand = null;
        Finger finger = null;

        if (handType == "left")
            hand = GetHand("left");
        else if (handType == "right")
            hand = GetHand("right");
        else
            Debug.LogError("No such hand type!");

        // Iterate through the fingers and find the thumb
        if (hand != null)
        {
            for (int f = 0; f < hand.Fingers.Count; f++)
            {
                finger = hand.Fingers[f];
                if (finger.Type.ToString() == fingerType)
                {
                    // Debug.Log ("Finger id: " + finger.Id + ", " + finger.Type.ToString () + " isExtended?: " + finger.IsExtended);
                    return finger;
                }
            }
        }
        else
        {
            return finger;
        }

        return finger;
    }
}
