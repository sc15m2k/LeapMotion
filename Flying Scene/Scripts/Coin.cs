﻿using UnityEngine;

public class Coin : MonoBehaviour {

    public int points = 10;

    void OnTriggerEnter(Collider col)
    {
        // If the player hits a coin
        if (col.gameObject.name == "PixelMakeVoyager_WithGuns")
        {
            // Get a target reference to the player
            GameObject hit = col.gameObject;
            Target target = hit.GetComponent<Target>();
            if (target != null)
            {
                // Add points to player's score
                target.AddPoints(points);
                Debug.Log("Collected by: " + target.name);
            }

            Destroy(gameObject);
        }
    }
}
