﻿using UnityEngine;

public class BulletBehaviour : MonoBehaviour {

    public int damage = 10;

    // Called when collision is first detected (on enter)
    void OnTriggerEnter(Collider col)
    {
        // If the player is hit by a bullet
        if (col.gameObject.name == "PixelMakeVoyager_WithGuns")
        {
            // Get a reference to the object that was hit
            GameObject hit = col.gameObject;
            // Get the target object that was hit
            Target target = hit.GetComponent<Target>();
            if (target != null)
            {
                // Apply damage to that object
                target.TakeDamage(damage);
                Debug.Log("Hit: " + target.name);
            }
        }
    }
}
