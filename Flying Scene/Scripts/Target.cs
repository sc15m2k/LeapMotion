﻿using UnityEngine;
using UnityEngine.UI;

public class Target : MonoBehaviour {

    //Player's health
    public const int maxHealth = 100;
    public int currentHealth = maxHealth;
    public bool isPlayer;
    public RectTransform healthBar;
    public Text scoreText;

    //Player's points
    private int points;

    public void TakeDamage(int amount)
    {
        //if the current health of an object drops below zero -> destroy it
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Debug.Log("Dead!");
            Destroy(gameObject);
        }

        if (isPlayer)
        {
            // We need to change the RectTransform’s Size Delta as a Vector2 to change the width
            healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
        }
    }

    public void AddPoints(int amount)
    {
        // Add points only if the object is the player
        if (isPlayer)
        {
            Debug.Log("Player's points: " + points);
            // Update player's points and update the text score on the screen
            points += amount;
            scoreText.text = "Score: " + points.ToString();
        }
    }
}
