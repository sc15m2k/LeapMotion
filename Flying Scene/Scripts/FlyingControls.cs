using UnityEngine;
using Leap;

public class FlyingControls : MonoBehaviour
{
    LeapBasicController controller;
    
    // Flying system
    [Tooltip("Freeze rotation on the x axis: cannot tilt hands up and down")]
    public bool FreezeRotationX;
    [Tooltip("Freeze rotation on the y axis: cannot move hands forward and back")]
    public bool FreezeRotationY;
    [Tooltip("Freeze rotation on the z axis: cannot move hands up and down")]
    public bool FreezeRotationZ;
    public float flyingSpeed = 50.0f;

    // Shooting system
    [Tooltip("Use raycasting to fire")]
    public bool fire;
    public int damage = 10;
    public Transform rightGun;
    public Transform leftGun;

    private float rollMult = 10.0f;
    private float range = 500f;
    private LineRenderer laserLine;
    private float fireRate = 0.5f;
    private float nextTimeToFire = 0f;

    void Start()
    {
        controller = new LeapBasicController();

        if (transform.parent == null)
        {
            Debug.LogError("No parent detected! Attach the script to a child of the object to be controlled.");
        }

        // Get and store a reference to our LineRenderer component
        laserLine = GetComponent<LineRenderer>();
        if (laserLine == null)
        {
            Debug.LogError("No line rendered detected");
        }
    }

    void FixedUpdate()
    {
        Frame frame = controller.GetFrame();
        Hand leftHand = controller.GetHand("left");
        Hand rightHand = controller.GetHand("right");

        if (frame != null)
        {
            if (leftHand != null && rightHand != null)
            {
                // takes the average vector of the forward vector of the hands, used for pitch of the plane.
                Vector3 palmDirectionVector = (leftHand.Direction.ToUnity() + rightHand.Direction.ToUnity()) * 0.5f;
                // palmDirectionVector.y = [-0.7 (going down) ~ 0.7 (going up)] - Leap Motion SDK
                // x axis rotation [-70 (going up) ~ 70 (going down)] - Unity (need a minus sign to invert it)

                // takes the hand difference to use rolling to turn.
                Vector3 palmDiff = leftHand.PalmPosition.ToUnityScaled() - rightHand.PalmPosition.ToUnityScaled();
                // palmDiff.z = [-7.0 ~ 7.0] - Leap Motion SDK (multiply by 3.0)
                // palmDiff.y = [-4.0 ~ 4.0] - Leap Motion SDK (multiply by rollMult)

                Vector3 newRotation = transform.parent.localRotation.eulerAngles;

                // x -> tilt hands up and down
                if (FreezeRotationX)
                    newRotation.x = 0.0f;
                else
                    newRotation.x = -(palmDirectionVector.y) * 100.0f;

                // y -> move hands forward and back
                if (FreezeRotationY)
                    palmDiff.z = 0.0f;

                // z -> move hands up and down
                if (FreezeRotationZ)
                    newRotation.z = 0;
                else
                    newRotation.z = -palmDiff.y * rollMult;

                newRotation.y += palmDiff.z * 3.0f - newRotation.z * 0.03f * transform.parent.GetComponent<Rigidbody>().velocity.magnitude;

                transform.parent.localRotation = Quaternion.Slerp(transform.parent.localRotation, Quaternion.Euler(newRotation), 0.1f);
                transform.parent.GetComponent<Rigidbody>().velocity = transform.parent.forward * flyingSpeed;

                if (fire)
                {
                    // Get the left thumb and check if it is extended or not (fire or not)
                    Finger leftThumb = controller.GetFinger("left", "TYPE_THUMB");
                    Finger rightThumb = controller.GetFinger("right", "TYPE_THUMB");
                    if ((leftThumb.IsExtended == false || rightThumb.IsExtended == false) && Time.time >= nextTimeToFire)
                    {
                        nextTimeToFire = Time.time + fireRate;
                        // Shoot with raycast
                        Raycast_Fire(rightGun);
                        laserLine.enabled = true;
                    }
                    else
                    {
                        laserLine.enabled = false;
                    }
                }
            }
            if (leftHand == null && rightHand == null)
            {
                //Reset the position and rotation of the plane - start again!
                transform.parent.position = new Vector3(0, 10, -10);
                transform.parent.rotation = Quaternion.identity;
            }
        }
    }

    // Shoot with raycast
    private void Raycast_Fire(Transform gun)
    {
        // Set the start position for our visual effect for our laser to the position of gunEnd
        laserLine.SetPosition(0, gun.position);

        RaycastHit hit;
        // Check if the raycast actually hits something along its way
        if (Physics.Raycast(gun.position, gun.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);
            // Set the end position for our laser line 
            laserLine.SetPosition(1, hit.point);

            // Get the target that was hit and apply damage to it
            Target target = hit.transform.GetComponent<Target>();
            if (target != null)
            {
                target.TakeDamage(damage);
            }
        }
        else
        {
            // If we did not hit anything, set the end of the line to a position directly in front of the camera at the distance of weaponRange
            laserLine.SetPosition(1, gun.position + (gun.transform.forward * range));
        }
    }
}
