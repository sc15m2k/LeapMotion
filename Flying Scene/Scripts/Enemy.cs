﻿using UnityEngine;

public class Enemy : MonoBehaviour {

    public Transform target;
    public float maxAttackDistance = 100f;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public float bulletSpeed = 10f;
    public float bulletLifetime = 2f;
    public float fireRate = 0.5f;

    private float nextTimeToFire = 0f;

    void FixedUpdate () {
        //Vector3 lookPos = target.position - transform.position;
        //lookPos.y = 0;
        //Quaternion rotation = Quaternion.LookRotation(lookPos);
        //transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.1f);

        if (target != null)
        {
            // calculate the distance between the enemy and the target (player)
            float distance = Vector3.Distance(target.position, transform.position);

            // make the enemy look at the target (player)
            Vector3 targetPostition = new Vector3(target.position.x, target.position.y, target.position.z);
            this.transform.LookAt(targetPostition);

            // if the target (player) is close enough to the enemy
            if (distance <= maxAttackDistance)
            {
                // and if the turret (enemy) has reloaded
                if (Time.time >= nextTimeToFire)
                {
                    nextTimeToFire = Time.time + fireRate;
                    Physics_Fire();
                }
            }
        }
        else
        {
            Debug.Log("The target is null!");
        }       
    }

    void Physics_Fire()
    {
        GameObject bullet = Instantiate(bulletPrefab);
        // Ignore the collision between the bullet and the weapon
        Physics.IgnoreCollision(bullet.GetComponent<Collider>(), bulletSpawn.parent.GetComponent<Collider>());
        // Spawn the bullet prefab on the bulletSpawn position
        bullet.transform.position = bulletSpawn.position;
        // Rotate the bullet so that it is coming out of the gun in the right angle
        Vector3 rotation = bullet.transform.rotation.eulerAngles;
        bullet.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);
        // Add force to the bullet and fire it
        bullet.GetComponent<Rigidbody>().AddForce(bulletSpawn.forward * bulletSpeed, ForceMode.Impulse);
        // Destroy the bullet after n seconds
        Destroy(bullet, bulletLifetime);
    }
}
